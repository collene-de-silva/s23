//CRUD OPERATIONS

//Insert Documents (CREATE)
/*
	Syntax: 
		Inserting One Document
			- db.collectionName.insertOne({
					"fieldA": "valueA",
					"fieldB": "valueB"
			})



*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});

//Shell - where you input commands in robo3t

/*

Insert Many Documents
	Syntax: 
		- db.collectionName.insertMany([
			{
				"fieldA": "valueA"
				"fieldB": "valueB"
			}
		])

*/

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"department": "none"
	}
]);

/*
	Mini Activity
		1. Make a new collection with the name courses
		2. Insert the following fields and values

			name: Javascript 101 
			price: 5000
			description: Introduction to Javascript 
			isActive: true

			name: HTML 101 
			price: 2000
			description: Introduction to HTML 
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS 
			isActive: false

*/

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": "5000",
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": "2000",
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": "2500",
		"description": "Introduction to CSS",
		"isActive": false
	},
])


//Find Documents (Read)
/*
	Syntax:
		-db.collectionName.find()
			-if no parameter, this will retrieve all documents

		-db.collectionName.find({"criteria": "value"})
			-will retrieve documents that match our criteria

		-db.collectionName.findOne({"criteria": "value"})
			-will retrieve the first document in the collection that match criteria

		-db.collectionName.findOne()
			-will return the first document in the collection
*/

db.users.find();

db.users.find({"firstName": "Jane"});

db.users.find(
	{"lastName": "Armstrong",
	"age": 82
	}
	);

//Updating documents (Update)
/*
	Syntax: 
		db.collectionName.updateOne(
			{
				"criteria": "field",
			},
			{
				$set: {
					"fieldToBeUpdated": "updatedValue"
				}
			}
		)

		db.collectionName.updateMany(
			{
				"criteria": "field",
			},
			{
				$set: {
					"fieldToBeUpdated": "updatedValue"
				}
			}
		)

		removing a field
		db.collectionName.updateOne(
			{
				"criteria": "field",
			},
			{
				$unset: {
					"fieldToBeRemoved": "value"
				}
			}
		)

		if targeting all: 
		{} - criteria

*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@gmail.com",
	"department": "none"
})

//Updating One Document
db.users.updateOne(
	{"firstName": "Test"},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "operation",
			"status": "active"
		}
	}
)

//Updating Multiple Documents 
db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
				"department": "HR"
			}
		}
	)

//Removing a field
db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$unset: {
				"status": "active"
			}
		}

	)



//Mini Activity 
/*
	1. Update the HTML 101 Course
	2. Make the isActive to false
	3. Add enrollees field to all the documents in our courses collections 
		Enrollees: 10
*/


db.courses.updateOne(
		{
			"name": "HTML 101"
		},
		{
			$set: {
				"isActive": false
			}
		}
	)

db.courses.updateOne(
		{
			"name": "Javascript 101"
		},
		{
			$set: {
				"enrollees": 10
			}
		}
	)

db.courses.updateOne(
		{
			"name": "HTML 101"
		},
		{
			$set: {
				"enrollees": 10
			}
		}
	)

db.courses.updateOne(
		{
			"name": "CSS 101"
		},
		{
			$set: {
				"enrollees": 10
			}
		}


db.courses.updateMany(
		{},
		{
			$set: {
				"enrollees": 10
			}
		}


	)

//Deleting Documents (DELETE)

//Document to delete 
db.users.insertOne({
	"firstName": "Test"
});


//Deleting a single document
/*
	Syntax:
		-db.collectionName.deleteOne({"criteria": "value"})
			-delete first docu with the criteria

		-db.collectionName.deleteOne({})
			-delete the first docu
*/

db.users.deleteOne({
	"firstName": "Test"
})

//Deleting multiple documents 
/*
	Syntax:
		-db.collectionName.deleteMany({"criteria":"value"})
*/

db.users.deleteMany({"department": "HR"})


db.courses.deleteMany({})